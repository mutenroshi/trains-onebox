package org.onebox.tests.trains.business.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.onebox.tests.trains.business.exceptions.TownsNotInRailroadMapException;
import org.onebox.tests.trains.business.model.Town;

/**
 * Class with the validations of the application. 
 * @author lucas
 *
 */
public class RailroadValidationUtil {
	
	/**
	 * Private constructor to avoid instantiation
	 */
	private RailroadValidationUtil() {}
	
	/**
	 * Validate if the towns map of the first parameter contains all the towns of the next parameters.
	 * @param towns map of towns
	 * @param tws towns to check
	 * @throws TownsNotInRailroadMapException 
	 */
	public static void validateTown(Map<String ,Town> towns, Town... tws) throws TownsNotInRailroadMapException {
		
		List<Town> badTowns = new ArrayList<>();
		
		for(Town tAux : tws) {
			if(!towns.containsValue(tAux)) {
				badTowns.add(tAux);
			}
		}
		
		if(!badTowns.isEmpty()) {
			throw new TownsNotInRailroadMapException(badTowns);
		}
	}
	
}
