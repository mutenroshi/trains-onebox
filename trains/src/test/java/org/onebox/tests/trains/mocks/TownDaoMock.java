package org.onebox.tests.trains.mocks;

import java.util.ArrayList;
import java.util.List;

import org.onebox.tests.trains.business.model.Town;
import org.onebox.tests.trains.dao.Dao;

public class TownDaoMock implements Dao<Town>{

	@Override
	public List<Town> getAll() {
		List<Town> towns = new ArrayList<>();
		towns.add(new Town("A", "Kaitaia"));
		towns.add(new Town("B", "Invercargill"));
		towns.add(new Town("C"));
		towns.add(new Town("D"));
		towns.add(new Town("E"));
		return towns;
	}

}
