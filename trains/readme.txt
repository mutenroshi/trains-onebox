
Assumptions:
	
	I have assumed that as the problem is expressed in English, the solution and documentation will be well received in English also.  

	To the development of this application I have assumed that the purpose is just to provide the code that solve the 3 problems exposed, as 
	the requirements says "The purpose of this problem is to help the railroad provide..." and not just "The purpose of this problem is to provide...".
	So i assumed that there was no need to create a web project with a UI, not 3 services, and nothing similar but just a class with 3 methods that could help for this kind of 
	projects. 
	
Use:

	To use this program just instantiate the class RailroadManager with a List of Routes using the classes Route and Town. 
	The class RailroadManager will provide 3 methods to solve the 3 problems exposed:
	1. Distance along a certain route.
	2. Number of different routes between two towns.
	3. Shortest route between two towns.
	This methods are overloaded so you can give them Town objects or just Strings with the id. The important is that the id of 
	the towns exists in the towns you used in the list of Routes at the moment you created the RailroadManager instance.  
	There are some extra methods that I created in order to solve the main 3 easily that are now usable. 
	
Test:
	
	I have used JUnit5 to create unit tests of the program, you can check how to execute the test in your favorite IDE 
	in the point 4 link: https://junit.org/junit5/docs/current/user-guide/#running-tests
	If you want to run just the test for the 3 main problem I tagged these with "required" so you can filter this tests
	when running with your IDE.  
	
Finally:
	Thanks for your time and, if you have anu questions that may arise, I am at your disposal. 