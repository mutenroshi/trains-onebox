package org.onebox.tests.trains.business.model;

/**
 * Class that represents a direct route betwheen two towns in one direction. 
 * Contains the two towns and the distance in between. 
 * @author lucas
 *
 */
public class Route {
	
	/**
	 * Origin of the route.
	 */
	private Town origin;
	
	/**
	 * Destination of the route
	 */
	private Town destination;
	
	/**
	 * Distance of the route
	 */
	private Double distance;
	
	/**
	 * @param origin
	 * @param destination
	 * @param distance
	 */
	public Route(Town origin, Town destination, Double distance) {
		super();
		this.origin = origin;
		this.destination = destination;
		this.distance = distance;
	}

	/**
	 * @return the origin
	 */
	public Town getOrigin() {
		return origin;
	}

	/**
	 * @param origin the origin to set
	 */
	public void setOrigin(Town origin) {
		this.origin = origin;
	}

	/**
	 * @return the destination
	 */
	public Town getDestination() {
		return destination;
	}

	/**
	 * @param destination the destination to set
	 */
	public void setDestination(Town destination) {
		this.destination = destination;
	}

	/**
	 * @return the distance
	 */
	public Double getDistance() {
		return distance;
	}

	/**
	 * @param distance the distance to set
	 */
	public void setDistance(Double distance) {
		this.distance = distance;
	}

	@Override
	public String toString() {
		return "[("+origin+" : " + destination + ") : " + distance;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((destination == null) ? 0 : destination.hashCode());
		result = prime * result + ((distance == null) ? 0 : distance.hashCode());
		result = prime * result + ((origin == null) ? 0 : origin.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Route other = (Route) obj;
		if (destination == null) {
			if (other.destination != null)
				return false;
		} else if (!destination.equals(other.destination))
			return false;
		if (distance == null) {
			if (other.distance != null)
				return false;
		} else if (!distance.equals(other.distance))
			return false;
		if (origin == null) {
			if (other.origin != null)
				return false;
		} else if (!origin.equals(other.origin))
			return false;
		return true;
	}

}
