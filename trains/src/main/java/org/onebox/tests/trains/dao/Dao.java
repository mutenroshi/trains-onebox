package org.onebox.tests.trains.dao;

import java.util.List;

/**
 * Generic Dao for the application.
 * @author lucas
 *
 * @param <T>
 */
public interface Dao<T> {
	/**
	 * Returns a list with all the information.
	 * @return List with all the registers in the table
	 */
	List<T> getAll();
}
