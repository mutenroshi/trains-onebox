package org.onebox.tests.trains.business;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.jgrapht.Graph;
import org.jgrapht.GraphPath;
import org.jgrapht.alg.shortestpath.AllDirectedPaths;
import org.jgrapht.alg.shortestpath.DijkstraShortestPath;
import org.jgrapht.graph.DefaultWeightedEdge;
import org.onebox.tests.trains.business.exceptions.TownsNotInRailroadMapException;
import org.onebox.tests.trains.business.model.Route;
import org.onebox.tests.trains.business.model.Town;
import org.onebox.tests.trains.business.util.RailroadUtil;
import org.onebox.tests.trains.business.util.RailroadValidationUtil;

/**
 * Main class of the application. Solves different problems like search for the shortest route from one town to another, 
 * count the total routes or get the distance of a particular route.
 * Note that to distinct a direct route represented in the class Route and a route that could have another towns in the middle 
 * of the journey in this class is used the term path instead of route.  
 * @author lucas
 *
 */
public class RailroadManager {
	
	/**
	 * Constant with the required message when a particular path does not exists. 
	 */
	public static final String NO_ROUTE_MSG = "NO SUCH ROUTE";
	
	/**
	 * Graph that represents the map of railroads.
	 */
	private Graph<Town, DefaultWeightedEdge> railroadGraph;
	
	/**
	 * Map of all the towns implied in the map of railroads.
	 */
	private Map<String ,Town> townsMap;
	
	/**
	 * Create an instance of RailroadManager with a list of routes.
	 * 
	 * @param routes list of routes that form the railroad. 
	 */
	public RailroadManager(List<Route> routes) {
		List<Town> towns = RailroadUtil.getTownsFromRoutes(routes);
		railroadGraph = RailroadUtil.constructGraph(routes, towns);
		townsMap = RailroadUtil.makeTownsMap(towns);
	}
	
	/**
	 * Gets the distance of a path, to introduce a path, put the towns as parameters in the order of the path.  
	 * If the path does not exist returns {@value #NO_ROUTE_MSG}
	 * @param towns Ordered sequence of towns that represents the path. 
	 * @return the distance if the path exists, otherwise {@value #NO_ROUTE_MSG}.
	 * @throws TownsNotInRailroadMapException 
	 */
	public String pathDistance(Town... towns ) throws TownsNotInRailroadMapException {
		
		RailroadValidationUtil.validateTown(this.townsMap, towns);
		
		double weight = 0;
		int i = 1;
		
		do {	
			if(railroadGraph.containsEdge(towns[i-1], towns[i])) {
				weight += railroadGraph.getEdgeWeight(railroadGraph.getEdge(towns[i-1], towns[i]));
			}else {
				weight = -1;
			}
			i++;
		}while(weight>=0 && i<towns.length);
				
		if(weight<0)
			return NO_ROUTE_MSG;
		else
			return String.valueOf(weight);
		
	}
	
	/**
	 * Gets the distance of a path, to introduce a path, put the townIds as parameters in the order of the path.  
	 * If the path does not exist returns {@value #NO_ROUTE_MSG}
	 * @param townIds Ordered String sequence of the id of the towns that represents the path. 
	 * @return the distance if the path exists, otherwise {@value #NO_ROUTE_MSG}.
	 * @throws TownsNotInRailroadMapException 
	 */
	public String pathDistance(String... townIds ) throws TownsNotInRailroadMapException {

		Town[] towns = new Town[townIds.length];
		
		for(int i = 0; i < towns.length; i++) {
			towns[i] = this.townsMap.get(townIds[i]);
		}
		
		return pathDistance(towns);
		
	}
	
	/**
	 * Gives a list with all the possibles paths between two towns, each path is represented 
	 * in an Array that contains the towns that forms the path in order.
	 * The Arrays will include the two towns of the query in the first (for the origin town) and
	 * the last (for the destination town) positions.  
	 * @param originId id of the first town
	 * @param destinationId id of the last town
	 * @return list of paths
	 * @throws TownsNotInRailroadMapException 
	 */
	public List<Town[]> allPaths(String originId, String destinationId) throws TownsNotInRailroadMapException{
		return allPaths(townsMap.get(originId), townsMap.get(destinationId));
	}
	
	/**
	 * Gives a list with all the possibles paths between two towns, each path is represented 
	 * in an Array that contains the towns that forms the path in order.
	 * The Arrays will include the two towns of the query in the first (for the origin town) and
	 * the last (for the destination town) positions.  
	 * @param origin first town
	 * @param destination last town
	 * @return list of paths
	 * @throws TownsNotInRailroadMapException 
	 */
	public List<Town[]> allPaths(Town origin, Town destination) throws TownsNotInRailroadMapException{
		
		RailroadValidationUtil.validateTown(this.townsMap, origin, destination);
		
		AllDirectedPaths<Town, DefaultWeightedEdge> paths = new AllDirectedPaths<>(railroadGraph);
		List<GraphPath<Town, DefaultWeightedEdge>> pathList = paths.getAllPaths(origin, destination, true, null);
		
		List<Town[]> townPaths = new ArrayList<>();
		for(GraphPath<Town, DefaultWeightedEdge> path : pathList) {
			Town[] towns =path.getVertexList().toArray(new Town[0]);
			townPaths.add(towns);
		}
		
		return townPaths;
	}
	
	/**
	 * Returns the total number of possibles paths between two towns.
	 * @param originId id of the first town
	 * @param destinationId id of the last town
	 * @return the number of towns
	 * @throws TownsNotInRailroadMapException 
	 */
	public Integer countPaths(String originId, String destinationId) throws TownsNotInRailroadMapException{
		return allPaths(townsMap.get(originId), townsMap.get(destinationId)).size();
	}
	
	/**
	 * Returns the total number of possibles paths between two towns.
	 * @param origin first town
	 * @param destination last town
	 * @return the number of towns
	 * @throws TownsNotInRailroadMapException 
	 */
	public Integer countPaths(Town origin, Town destination) throws TownsNotInRailroadMapException{
		return allPaths(origin, destination).size();
	}
	
	/**
	 * Returns the shortest path in distance (not in number of towns) betwhen two towns. 
	 * @param originId id of the first town
	 * @param destinationId id of the last town
	 * @return shortest path represented with an ordered Array that includes both first and last town.
	 * @throws TownsNotInRailroadMapException 
	 */
	public Town[] shortestPath(String originId, String destinationId) throws TownsNotInRailroadMapException {
		return shortestPath(townsMap.get(originId), townsMap.get(destinationId));
	}
	
	/**
	 * Returns the shortest path in distance (not in number of towns) betwhen two towns. 
	 * @param origin first town
	 * @param destination last town
	 * @return shortest path represented with an ordered Array that includes both first and last town.
	 * @throws TownsNotInRailroadMapException 
	 */
	public Town[] shortestPath(Town origin, Town destination) throws TownsNotInRailroadMapException {
		RailroadValidationUtil.validateTown(this.townsMap, origin, destination);
		DijkstraShortestPath<Town, DefaultWeightedEdge> dsp = new DijkstraShortestPath<>(railroadGraph);
		GraphPath<Town, DefaultWeightedEdge> shortestPathBE = dsp.getPath(origin, destination);
		
		return shortestPathBE.getVertexList().toArray(new Town[0]);
	}
	
	/**
	 * @return the railroad
	 */
	public Graph<Town, DefaultWeightedEdge> getRailroad() {
		return railroadGraph;
	}

	/**
	 * @param railroad the railroad to set
	 */
	public void setRailroad(Graph<Town, DefaultWeightedEdge> railroad) {
		this.railroadGraph = railroad;
	}

	/**
	 * @return the townsMap
	 */
	public Map<String, Town> getTownsMap() {
		return townsMap;
	}

	/**
	 * @param townsMap the townsMap to set
	 */
	public void setTownsMap(Map<String, Town> townsMap) {
		this.townsMap = townsMap;
	}

	
}
