package org.onebox.tests.trains.business.model;

/**
 * Class that represents a town by an id and a name.
 * The name is optional in the execution of this application. 
 * @author lucas
 *
 */
public class Town {
	
	/**
	 * Identification of the town, it must be a single capital letter.
	 */
	private String id;
	
	/**
	 * Name of the city
	 */
	private String name;
	
	/**
	 * @param id
	 */
	public Town(String id) {
		super();
		this.id = id;
	}

	/**
	 * @param id
	 * @param name
	 */
	public Town(String id, String name) {
		super();
		this.id = id;
		this.name = name;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	@Override
	public String toString() {
		return this.id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Town other = (Town) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
}
