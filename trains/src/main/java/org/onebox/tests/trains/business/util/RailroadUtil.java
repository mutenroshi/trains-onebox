package org.onebox.tests.trains.business.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jgrapht.Graph;
import org.jgrapht.graph.DefaultWeightedEdge;
import org.jgrapht.graph.SimpleDirectedWeightedGraph;
import org.onebox.tests.trains.business.model.Route;
import org.onebox.tests.trains.business.model.Town;

/**
 * Utility class to assist generic methods to adapt information for the application.  
 * @author lucas
 *
 */
public final class RailroadUtil {
	
	/**
	 * Private constructor to avoid instantiation
	 */
	private RailroadUtil() {}
	
	/**
	 * Provide a graph of towns and routes.
	 * 
	 * @param routes list of routes that will be represented as edges of the graph
	 * @param towns list of towns that will be represented as vertex of the graph
	 * @return the graph built with the routes and towns  
	 */
	public static Graph<Town, DefaultWeightedEdge> constructGraph(List<Route> routes, List<Town> towns) {
		Graph<Town, DefaultWeightedEdge> railroad = new SimpleDirectedWeightedGraph<>(DefaultWeightedEdge.class);
		
		for(Town t : towns) {
			railroad.addVertex(t);
		}
		
		for(Route r : routes) {
			DefaultWeightedEdge edge = railroad.addEdge(r.getOrigin(),  r.getDestination());
			railroad.setEdgeWeight(edge, r.getDistance());
		}
		return railroad;
	}

	/**
	 * Provide a map of towns with the town as the value and the town Id as the key.
	 * 
	 * @param towns towns that will be used in the application in a List format
	 * @return towns that will be used in the application in a Map format 
	 */
	public static Map<String ,Town> makeTownsMap(List<Town> towns) {
		Map<String ,Town> townsMap = new HashMap<>();
		for(Town t: towns) {
			townsMap.put(t.getId(), t);
		}
		return townsMap;
	}

	/**
	 * Return a list with all the towns find in the different routes.
	 * @param routes List of routes.
	 * @return List with the towns in the routes.  
	 */
	public static List<Town> getTownsFromRoutes(List<Route> routes) {

		List<Town> towns = new ArrayList<>();
		
		for(Route r : routes) {
			if(!towns.contains(r.getOrigin())) {
				towns.add(r.getOrigin());
			}
			
			if(!towns.contains(r.getDestination())) {
				towns.add(r.getDestination());
			}

		}
		
		return towns;
	}
}
