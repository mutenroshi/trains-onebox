package org.onebox.tests.trains.mocks;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.onebox.tests.trains.business.model.Route;
import org.onebox.tests.trains.business.model.Town;
import org.onebox.tests.trains.business.util.RailroadUtil;
import org.onebox.tests.trains.dao.Dao;

public class RouteDaoMock implements Dao<Route> {

	public static final String A = "A";
	public static final String B = "B";
	public static final String C = "C";
	public static final String D = "D";
	public static final String E = "E";
	
	@Override
	public List<Route> getAll() {
		
		Map<String ,Town> townsMap = RailroadUtil.makeTownsMap(new TownDaoMock().getAll());
		
		Town a = townsMap.get(A);
		Town b = townsMap.get(B);
		Town c = townsMap.get(C);
		Town d = townsMap.get(D);
		Town e = townsMap.get(E);
		
		List<Route> routes = new ArrayList<>();
		routes.add(new Route(a, b, 5D));
		routes.add(new Route(b, c, 4D));
		routes.add(new Route(c, d, 8D));
		routes.add(new Route(d, c, 8D));
		routes.add(new Route(d, e, 6D));		
		routes.add(new Route(a, d, 5D));
		routes.add(new Route(c, e, 2D));
		routes.add(new Route(e, b, 3D));
		routes.add(new Route(a, e, 7D));
		
		return routes;
	}
	
}
