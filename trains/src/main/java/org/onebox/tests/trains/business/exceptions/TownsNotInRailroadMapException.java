package org.onebox.tests.trains.business.exceptions;

import java.util.List;

import org.onebox.tests.trains.business.model.Town;

/**
 * Exception to manage incorrect Towns inputs
 * @author lucas
 *
 */
public class TownsNotInRailroadMapException extends Exception {


	/**
	 * generated serial version UID
	 */
	private static final long serialVersionUID = -7367034013152113760L;
	
	/**
	 * Constructor with the towns that do not exists in the map. 
	 * @param badTowns
	 */
	public TownsNotInRailroadMapException(List<Town> badTowns) {
		super("The Railroad map contains the following non existing towns: " + badTowns.toString());
	}

}
