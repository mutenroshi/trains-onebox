package org.onebox.tests.trains;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;
import org.junit.jupiter.api.TestReporter;
import org.onebox.tests.trains.business.RailroadManager;
import org.onebox.tests.trains.business.exceptions.TownsNotInRailroadMapException;
import org.onebox.tests.trains.business.model.Town;
import org.onebox.tests.trains.mocks.RouteDaoMock;

@DisplayName("Railroad class")
class RailroadTest {
	
	TestInfo testInfo;
	TestReporter testReporter;
	RailroadManager railroad;
	
	@BeforeEach
	void initTest(TestInfo testInfo, TestReporter testReporter) {
		this.testInfo = testInfo;
		this.testReporter = testReporter;
		railroad = new RailroadManager(new RouteDaoMock().getAll());
	}
	
	@Test
	@DisplayName("route distance method")
	void testRouteDistance() {
		testReporter.publishEntry("Running " + testInfo.getDisplayName());
		Town a = railroad.getTownsMap().get(RouteDaoMock.A);
		Town b = railroad.getTownsMap().get(RouteDaoMock.B);
		Town c = railroad.getTownsMap().get(RouteDaoMock.C);
		Town d = railroad.getTownsMap().get(RouteDaoMock.D);
		Town e = railroad.getTownsMap().get(RouteDaoMock.E);
		
		assertAll(
				() -> assertEquals("9.0", railroad.pathDistance(a, b, c)),
				() -> assertEquals("5.0", railroad.pathDistance(a, d)),
				() -> assertEquals("13.0", railroad.pathDistance(a, d, c)),
				() -> assertEquals("22.0", railroad.pathDistance(a, e, b, c, d)),
				() -> assertEquals(RailroadManager.NO_ROUTE_MSG, railroad.pathDistance(a, e, d))
				);
	}
	
	@Test
	@DisplayName("route distance method (Strings params)")
	@Tag("required")
	void testRouteDistanceStr() {
		testReporter.publishEntry("Running " + testInfo.getDisplayName());
		String a = railroad.getTownsMap().get(RouteDaoMock.A).getId();
		String b = railroad.getTownsMap().get(RouteDaoMock.B).getId();
		String c = railroad.getTownsMap().get(RouteDaoMock.C).getId();
		String d = railroad.getTownsMap().get(RouteDaoMock.D).getId();
		String e = railroad.getTownsMap().get(RouteDaoMock.E).getId();
		
		assertAll(
				() -> assertEquals("9.0", railroad.pathDistance(a, b, c)),
				() -> assertEquals("5.0", railroad.pathDistance(a, d)),
				() -> assertEquals("13.0", railroad.pathDistance(a, d, c)),
				() -> assertEquals("22.0", railroad.pathDistance(a, e, b, c, d)),
				() -> assertEquals(RailroadManager.NO_ROUTE_MSG, railroad.pathDistance(a, e, d))
				);
	}
	
	@Test
	@DisplayName("all paths method")
	void testAllPaths() throws TownsNotInRailroadMapException {
		testReporter.publishEntry("Running " + testInfo.getDisplayName());

		List<Town[]> routes = railroad.allPaths(railroad.getTownsMap().get(RouteDaoMock.A), 
				railroad.getTownsMap().get(RouteDaoMock.B));
		assertAll(
				() -> assertEquals("[A, B]", Arrays.toString(routes.get(0))),
				() -> assertEquals("[A, D, E, B]", Arrays.toString(routes.get(1))),
				() -> assertEquals("[A, D, C, E, B]", Arrays.toString(routes.get(2))),
				() -> assertEquals("[A, E, B]", Arrays.toString(routes.get(3)))
				);

	}
	
	@Test
	@DisplayName("all paths method (String params)")
	void testAllPathsStr() throws TownsNotInRailroadMapException {
		testReporter.publishEntry("Running " + testInfo.getDisplayName());

		List<Town[]> routes = railroad.allPaths(railroad.getTownsMap().get(RouteDaoMock.A).getId(), 
				railroad.getTownsMap().get(RouteDaoMock.B).getId());
		assertAll(
				() -> assertEquals("[A, B]", Arrays.toString(routes.get(0))),
				() -> assertEquals("[A, D, E, B]", Arrays.toString(routes.get(1))),
				() -> assertEquals("[A, D, C, E, B]", Arrays.toString(routes.get(2))),
				() -> assertEquals("[A, E, B]", Arrays.toString(routes.get(3)))
				);

	}
	
	@Test
	@DisplayName("count paths method")
	void testCountPaths() throws TownsNotInRailroadMapException {
		testReporter.publishEntry("Running " + testInfo.getDisplayName());	
		assertEquals(4, railroad.countPaths(railroad.getTownsMap().get(RouteDaoMock.A), 
				railroad.getTownsMap().get(RouteDaoMock.B)));
	}
	
	@Test
	@DisplayName("count paths method (String params)")
	@Tag("required")
	void testCountPathsStr() throws TownsNotInRailroadMapException {
		testReporter.publishEntry("Running " + testInfo.getDisplayName());	
		assertEquals(4, railroad.countPaths(railroad.getTownsMap().get(RouteDaoMock.A).getId(), 
				railroad.getTownsMap().get(RouteDaoMock.B).getId()));
	}
	
	@Test
	@DisplayName("shortest path method")
	void testShortestPath() throws TownsNotInRailroadMapException {
		testReporter.publishEntry("Running " + testInfo.getDisplayName());	
		assertEquals("[B, C, E]", Arrays.toString(railroad.shortestPath(railroad.getTownsMap().get(RouteDaoMock.B),
				railroad.getTownsMap().get(RouteDaoMock.E))));
	}
	
	@Test
	@DisplayName("shortest path method  (String params)")
	@Tag("required")
	void testShortestPathStr() throws TownsNotInRailroadMapException {
		testReporter.publishEntry("Running " + testInfo.getDisplayName());	
		assertEquals("[B, C, E]", Arrays.toString(railroad.shortestPath(railroad.getTownsMap().get(RouteDaoMock.B).getId(),
				railroad.getTownsMap().get(RouteDaoMock.E).getId())));
	}
	
	@Test
	@DisplayName("towns not in railroad map exception")
	void testTownsNotInRailroadMapException(){
		testReporter.publishEntry("Running " + testInfo.getDisplayName());	
		Town imaginaryTown1 = new Town("K");
		Town imaginaryTown2 = new Town("T");
		assertAll(
				() -> assertThrows(TownsNotInRailroadMapException.class, () -> railroad.pathDistance(imaginaryTown1, imaginaryTown2), "Towns that not exist should throw"),
				() -> assertThrows(TownsNotInRailroadMapException.class, () -> railroad.countPaths(imaginaryTown1, imaginaryTown2), "Towns that not exist should throw"),
				() -> assertThrows(TownsNotInRailroadMapException.class, () -> railroad.shortestPath(imaginaryTown1, imaginaryTown2), "Towns that not exist should throw"),
				() -> assertThrows(TownsNotInRailroadMapException.class, () -> railroad.pathDistance("L", "T"), "Towns that not exist should throw"),
				() -> assertThrows(TownsNotInRailroadMapException.class, () -> railroad.countPaths("F", "M"), "Towns that not exist should throw"),
				() -> assertThrows(TownsNotInRailroadMapException.class, () -> railroad.shortestPath("O", "F"), "Towns that not exist should throw")
				);
	}
}
